import binascii
from AptUrl.Helpers import _

from odoo import models, fields, api, http, SUPERUSER_ID
from odoo.exceptions import AccessError, MissingError
from odoo.http import request
from odoo.tools import consteq


class ControllerSignature(http.Controller):

    @http.route('/test1/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route(['/my/invoices/<int:invoice_id>/test'], type='json', auth="public", website=True)
    def portal_quote_accept(self, invoice_id, access_token=None, name=None, signature=None):
        # get from query string if not on json param
        access_token = access_token or request.httprequest.args.get('access_token')
        try:
            invoice_sudo = self._document_check_access('account.move', invoice_id, access_token=access_token)
        except (AccessError, MissingError):
            return {'error': _('Invalid order.')}

        if not signature:
            return {'error': _('Signature is missing.')}

        if not invoice_sudo.has_to_be_signed(True):
            return {'error': _('The order is not in a state requiring customer signature.')}

        try:
            invoice_sudo.write({
                'signed_by': name,
                'signed_on': fields.Datetime.now(),
                'signature': signature,
            })
            request.env.cr.commit()
        except (TypeError, binascii.Error) as e:
            return {'error': _('Invalid signature data.')}
        return {
            'force_refresh': True,
        }



    def _document_check_access(self, model_name, document_id, access_token=None):
        document = request.env[model_name].browse([document_id])
        document_sudo = document.with_user(SUPERUSER_ID).exists()
        if not document_sudo:
            raise MissingError(_("This document does not exist."))
        try:
            document.check_access_rights('read')
            document.check_access_rule('read')
        except AccessError:
            if not access_token or not document_sudo.access_token or not consteq(document_sudo.access_token, access_token):
                raise
        return document_sudo
