from odoo import models, fields, api, http


class InvoicingSignature(models.Model):
    _inherit = 'account.move'
    _description = 'Invoicing Signature Inherit'

    signed_by = fields.Char(string="Signed By")
    signed_on = fields.Datetime('Signed On', help='Date of the signature.', copy=False)
    signature = fields.Binary(string="Customer Signature", store=True, readonly=True)

    def has_to_be_signed(self, include_draft=False):
        return (self.state == 'posted' or (self.state == 'draft' and include_draft)) and not self.signature

    def has_to_be_paid(self, include_draft=False):
        transaction = self.get_portal_last_transaction()
        return (self.state == 'posted' or (
                self.state == 'draft' and include_draft)) and transaction.state != 'done' and self.amount_total
